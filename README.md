# BPK Config 2022 #
Official BPK CS:GO config




## Installation Steps

1. [Click here to download](https://bitbucket.org/arienshibani/verdens-beste-cs-config/get/master.zip) the .cfg files from this repo. 
2. Put both *autoexec.cfg* and *practice.cfg* in `...\Steam\steamapps\common\Counter-Strike Global Offensive\csgo\cfg`
2. Set up your launch options by right cliking CS:GO in steam -> properties -> Set launch options
    
    * Enter the following `+exec autoexec.cfg -novid -tickrate 128 -high`

3. Start your game, and check your console. There should be a confirmation that the BPK Config has been loaded.

# autoexec.cfg
The BPK autoexec configuration contains the following features. All default bindings can be edited to your own preferences. 

* Caps Lock - **Insta Drop Bomb Script**  (NEW!!)
    * Instantly switch to the bomb, and drop it with a single button. 
* F1 - **Clutch Mode** (toggle)
    * The infamous BPK clutch mode. Press it to mute your whole team. Press it again to unmute.
* F2 - noclip (toggle)
    * Toggle noclip on and off, usefull when practicing nades. 
    * requires *sv_cheats 1* to work.
* V - Jump Throw
    * While holding in a grenade, press V to jump and throw your grenade at the same time.
    * Some smokes can only be thrown using this script. 
* X - Fullscreen Crosshair (Active when held down)
    * Extremely usefull for intricate and detailed smoke/molotov lineups.
* Tab - Show FPS and MS (Active when held down)
    * Shows your FPS and MS (net_graph), only when TAB is held down.
* L - Super Spin Mode (Toggle)
    * Sets your sensitivty up real high, so you can do big spins for the lulz.
* R - Auto remove decals, everytime your reload

It also contains some basic settings for your viewmodel and crosshair to get you started, feel free to replace them with your own.

# practice.cfg
Contains all the pre-configured settings you need for a propper smoke/molotov training session.
Unlimited nades, unlimited HP, sv_cheats set to 1 (so you can easily use your noclip script). To enable practice mode try the following.

1. Enter any map you wish to practice your smokes on
    * `map de_dust2`

2. Type in the following in your console afterwards
    * `exec practice.cfg`

3. Thats it! All bots should be kicked, and you can start throwing unlimited nades for as long as you want.    

### Changelog
BPK Config is constantly updated. Changelogs can be found under revisions [here](https://bitbucket.org/modalitsu/verdens-beste-cs-config/commits/) 


